use inc::Module::Install;

name 'Zaaksysteem-InstanceConfig';

author 'Rudolf Leermakers <rudolf@mintlab.nl>';
version '0.01';
license 'eupl';
perl_version '5.010';

requires map { $_ => 0 } qw[
    JSON
    Config::General
    Config::Any
    Moose
    File::Spec
    List::MoreUtils
    MooseX::Log::Log4perl
    Redis
];

WriteAll;

1;
