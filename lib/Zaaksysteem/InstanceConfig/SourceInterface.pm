package Zaaksysteem::InstanceConfig::SourceInterface;

use Moose::Role;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::InstanceConfig::SourceInterface - Interface defintion for
configuration suppliers

=head1 DESCRIPTION

This interface defines the common API for loading of configuration
(customer_d) data.

    package Zaaksysteem::InstanceConfig::DescriptiveImplementationName

    use Moose;

    with 'Zaaksysteem::Instance::Config::SourceInterface';

    ...

=cut

requires qw[
    find_instance_config_by_hostname
    all_instance_hosts
    flush
];

sig find_instance_config_by_hostname => 'Str => HashRef';

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
