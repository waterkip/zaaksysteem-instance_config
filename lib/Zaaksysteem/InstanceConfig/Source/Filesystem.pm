package Zaaksysteem::InstanceConfig::Source::Filesystem;

use Moose;
use namespace::autoclean;

use File::Spec::Functions;
use Config::Any;
use Zaaksysteem::Tools;
use List::MoreUtils qw(uniq);

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::InstanceConfig::Source::Filesystem - File-based instance
configuration source

=head1 SYNOPSIS

    my $configs = Zaaksysteem::InstanceConfig::Source::Filesystem->new(
        path => '/etc/zaaksysteem/customer.d'
    );

    my @instance_config_hosts = $configs->all_instance_hosts();

    foreach (@instance_config_hosts) {
        my $config = $configs->find_instance_config_by_hostname($_);

        ...
    }

=head1 ATTRIBUTES

=head2 path

Path to a directory containing instance configuration definitions. Required.

=cut

has path => (
    is       => 'ro',
    isa      => 'Str',
    required => 1
);

=head2 instance_configurations

Customers configuration which are found in the customer.d directory.

=head3 Methods

This attribute exposes delegated methods

=over 4

=item all_instance_configs

Retrieve all instance configurations as a list of values. Delegate of
L<Moose::Meta::Attribute::Native::Trait::Hash/values>.

=item all_instance_hosts

Retrieve all hostnames of the configured instances. Delegate of
L<Moose::Meta::Attribute::Native::Trait::Hash/keys>.

=item find_instance_config_by_hostname

Retrieve a specific instance configuration by hostname. Delegate of
L<Moose::Meta::Attribute::Native::Trait::Hash/get($key, $key2, $key3...)>.

=back

=cut

has instance_configurations => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy    => 1,
    traits  => [qw[Hash]],
    builder => 'build_instance_configurations',
    handles => {
        all_instance_configs => 'values',
        all_instance_hosts => 'keys',
        find_instance_config_by_hostname => 'get',
        clear_instance_configs => 'clear'
    }
);

=head1 METHODS

=head2 flush

=cut

sub flush {
    my $self = shift;

    #$self->clear_instance_configs;

    return;
}

=head1 PRIVATE METHODS

=head2 build_instance_configurations

Loads all the configuration files found in the L</path>.

=head3 RETURNS

HashRef on succes, dies on failure or if no instance configuration
declarations are found.

=cut

sub build_instance_configurations {
    my $self = shift;

    my $path = $self->path;

    unless (-d $path) {
        throw('instance_config/filesystem/path_not_found', sprintf(
            'Failed to load instance configurations in "%s"',
            $path
        ));
    }

    my $cfgs = Config::Any->load_files(
        {
            files           => [glob(catfile($path, '*.conf'))],
            use_ext         => 1,
            flatten_to_hash => 1,
            driver_args     => {
                General => {
                    -ForceArray => 1
                },
            },
        }
    );

    my %domains;

    for my $configfile (values %{$cfgs}) {
        for my $instance (keys %{$configfile}) {
            if ($configfile->{$instance}{disabled}) {
                $self->log->info(sprintf(
                    'Instance "%s" is disabled, skipping',
                    $instance
                ));

                next;
            }

            my $aliases = delete $configfile->{$instance}{aliases};
            my @hosts = $aliases ? split(/[\,\;]\s*/, $aliases) : ();

            foreach my $host (uniq(@hosts, $instance)) {
                if (exists $domains{$host}) {
                    $self->log->warn(sprintf(
                        'Instance hostname "%s" already defined, skipping',
                        $host
                    ));

                    next;
                }

                $domains{$host} = $configfile->{$instance};
            }
        }
    }

    return \%domains;
}

# Apply interface role after setting up 'instance_configurations' so the required
# method 'find_instance_config_by_hostname' exists.
with 'Zaaksysteem::InstanceConfig::SourceInterface';

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
